set(sources
  pqCMBPostProcessingModeBehavior.cxx
  pqCMBPostProcessingModeBehavior.h)

set(rc_files
  pqCMBPostProcessingModeBehavior.qrc)

set(ui_files
  pqCMBPostProcessingModeBehavior.ui)

set(xml_files
  postprocessing-filters.xml
  postprocessing-sources.xml)

set(interfaces)
paraview_plugin_add_action_group(
  CLASS_NAME pqCMBPostProcessingModeBehavior
  GROUP_NAME "ToolBar/PostProcessing"
  INTERFACES group_interfaces
  SOURCES group_sources)
list(APPEND interfaces
  ${group_interfaces})
list(APPEND sources
  ${group_sources})

# Now mush it all into a plugin:
paraview_add_plugin(cmbPostProcessingModePlugin
  FORCE_STATIC ON
  VERSION "1.0"
  UI_INTERFACES ${interfaces}
  UI_FILES ${ui_files}
  UI_RESOURCES ${rc_files}
  SOURCES ${sources})
target_link_libraries(cmbPostProcessingModePlugin
  LINK_PRIVATE
    ParaView::pqApplicationComponents
    ParaView::pqCore
)
