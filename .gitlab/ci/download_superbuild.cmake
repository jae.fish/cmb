cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20210420
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  set(file_item "60805ad32fa25629b9ff8e8f")
  set(file_hash "c1eba717d4d113bb70b41fbeafea7f345175e07777d797057bc3fe4952777a2ecfefb746f52bcfccd96ec8b48e50fb69e3b0b8b718e5f04eea9fc26affa35cef")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  set(file_item "60805aa92fa25629b9ff8e61")
  set(file_hash "19e8081878578cd5f1d408828f3183676aa28f23094d4a142f87b61078b49421b522389354369e9665671dcdb76ff0d3714477d073cac3f72a0a6f75e2fed0bc")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
