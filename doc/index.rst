################################
CMB: Computational Model Builder
################################
Version: |release|

.. highlight:: c++

.. role:: cxx(code)
   :language: c++

.. findimage:: cmb-logo.*

Contents:

.. toctree::
   :maxdepth: 4

   userguide/index.rst

##################
Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
