CAD model support
=================

Modelbuilder now comes with support to load IGES, STEP, and OpenCascade
files via OpenCascade_ . Only viewing is currently supported.
In the future, annotations on CAD models as well as modeling operations
will be added.

.. _OpenCascade: https://www.opencascade.com/
